# Project GAYA by NYM Srl 

this Repo includes the documentation for the GAYA project.

The documentation is organised as follows:

- Productisation:
  - GAYA-Business-KPI - Vision Mission Strategy
  - Gaya-business case: 500 words Business case for Gaya project
  - GAYA-project summary
  - GAYA-collaboration-with-ssi-business-ecosystem
  - Gaya-Technical-work-programme
-  business-open-call-1-productisation-meeting

For more information, please contact: 

Gitlab: @egidio.casati

Email: egidio.casati@nymlab.it

