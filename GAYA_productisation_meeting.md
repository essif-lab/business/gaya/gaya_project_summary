
# Template for Deliverable for the Pruductisation meeting of GAYA

Contact person details:
**Egidio Casati**
egidio.casati@nymlab.it
+393459277339
linkedin: [https://www.linkedin.com/in/egidiocasati/](https://www.linkedin.com/in/egidiocasati/)
home: [https://www.nymlab.it](https://www.nymlab.it)


## Business Page

### 1. Define your product
Gaya is the technological platform that allows you to digitize the process of signing agreements remotely ensuring the characteristics of de visu meetings in terms of reconsideration, ease of signature and event control.
Gaya leverage SSI technology in order to provide subscribers with an Identity Wallet. This could unlock a new digital life for the company and its shareholders, that could gain access to blockchain powered service, proving their identity and preserving their privacy as both physical and juridical persons.

### 2. Value proposition – what is/are the benefit(s) for your customer

 - The management of specific files by professional category through a predefined workflow directory  
 - Management of virtual presence at the conclusion of agreements
 - Certification of the identity of the participants in maximum security
 - The management of the signing of agreements by means of the electronic signature procedures provided for by law
 - Recording and storage of all relevant documentation and
   events along the file flow

### 3. Who are your customers
The initial idea of Gaya is to support the digitization of the limited Liability Company incorporation public act. We explored and tested the market via interview and involvement of several professionals and we discovered that Gaya could serve a broader market, such as:

**Notaries**
 - Public acts
 - authenticated private writings
 - simple private writings

 **Lawyers**
- power of attorney
- private writing

**Accountants**
- mandates
- anti-money laundering
- Extraordinary operations
- Sign company documents

**Insurance**
- Insurance policies
- Customer Dossier

**Real Estate Agencies**
- Rental assignments
- Sales appointments
- leases
- sales proposals

**Financial intermediaries**
- mortgage applications
- loan applications

**Banks**
- mandates
- Client Documents

**tax advice centres**
- tax returns
- Customer Dossiers
- Taxes

### 4. What is your targeted time to market (for a product on the market)

 - January 2021 with a Family&Friends beta test
 - April 2021 ready for the open Market

### 5. What is the business model
Gaya is a SaaS platform sold as:
- subscription based model
- enterprise project base model
Selling channel:
- eCommerce website, providing about 3 different packages:
	- Smart
	- Studio
	- Notary
- Business development/Account managers, for Enterprise projects

### 6. Who will be your first customer (do you have a first customer?)
we are trying to set up a partnership agreement with an interested company. They could provide a mix of risk capital and work for equity. They could also be the first customer as they are a financial intermediary agencies with about 500 operators.

### 7. Do you plan to raise funds through private investors ?
Yes

### 8. Have you already initiated contact with Venture Capital firms ?

we are already in contact with Business Angels

## Technical Page
<!-- While creating contents for the following sections, please try to keep this section limited to a single page-->

### Please provide answers to the following questions before the productisation meeting:

1. What is the status of your product development?
We tested different components such as:
- Opensource internal video conferencing platform
- Qualified Digital Signature Service
and we're in the process of delivering a first Demo that shows how 2 subscribers and a notary could carry out a public act.
We're also defining the detailed architcture and the relative detailed implementation plan.

2. Are you aware of the eSSIF-Lab Library? What are the functionalities you would like to use?

We're not aware of the eSSIF-Lab library feature yet. Since our experience in the SSI space, we expect to use the library to build Identity wallet for the Holder, issuer and verifier and we expct to use Gaya as an:
- Issuer, when the subscriber doesn't have a trusted identity credential yet
- verifier, when the subscriber already has a trusted identity credential

### To be discussed duing the productisation meeting:

1. eSSIF-Lab partnerships: (see matchmaking process)
   1. what do you have on offer to other participants ?

We can offer a business vertical active as verifier of identity credential issued by trusted partner.

   2. what would you like other participants to offer you ?

We can offer a SSI powered platform to legally sign agreement

2. How does your product fit within the eSSIF-Lab vision and architecture ?

We think e can perfectly fit as a business vertical acting both as issuer or verifier of identity credential

3. What is the timeline for the technical development ? Respect the milestones/timing… define the KPIs.

Phase 1 - Productisation
in this initial phase we plan to deliver:
- a working demo, PoC of the platform, intergating:
	- Qualified Electronic Signature (test environment) provided by an accredited QTSP
	- OpenSource Video Conferencing Platform
	- process Workflow
- the technical description of the eSSIF Lab Library Integration
- the integration with other eSSIF-Lab BOC Participant (SSI4DTM and ComKYC)
- the Business Canvas
