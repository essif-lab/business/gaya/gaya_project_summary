# BOC-1 - Project Summary of GAYA

For more information, please contact: 

Gitlab: @egidio.casati

Email: egidio.casati@nymlab.it

## Summary

GAYA is the solution that allows public notaries (and other professionals and companies) to digitize the process of making agreements remotely, granting the same features of de visu meetings in terms of recognition, easy signing and event control.

GAYA leverages SSI protocols in order to:

- Promote the role of professionals (such as notaries) as issuers of verifiable credentials
- Ease the process of Identity Verification and Power of acting on behalf of a company by its users

GAYA solution pillars are:

- Identification, via Verifiable Credential validation or KYC process
- Collaboration, via an internal dedicated videoconference and document sharing components
- Signature, supporting all the eIDAS signature types (electroninc signature, advanced electronic signature and qualified electronic signature)
- Audit and management, thanks to the notarization and archival of all the relevant events of an agreement signing procedure.

Last but not least, GAYA shall focus on a innovative concept of Agreement in the form of a Ricardian Contract, which shall be a legally binding and valid document.

## Business Problem

By August 2021, every member state shall allow the online incorporation of Limited Liability Company, without the involvement of a Public Notary, as provisioned  by the norms:

- EU2017/1132 – general provisions regarding the establishment and functioning of limited liability companies.
- EU2019/1151 – regards the use of digital tools and processes in company law

Public notaries category (particularly in Italy) is in turmoil and is animated by a lively debate on the issue: many see this as a problem as they are hostile to the Digital online Act, others see this as an incredible opportunity to innovate their profession.

When the covid-19 entered the scene we realized that the digital transformation gap to be filled was much wider than we thought. In addition, the urgency of such a transformation could have introduced enormous risks for privacy.

Looking at the european market we noticed that:

- there is a very poor knowledge of the European Digital Trust Market brought by eIDAS and the opportunity to fill the digital transformation gap
- there is even less awarness of the opportunities that comes from adoption of SSI approach, particularly:
  - the chance to play an important role as issuer, by most of the potentially involved professionals
  - the chance to re-use Identity verifiable credential in most of the KYC process
- There's a missing link between the day-to-day work patterns of such professionals, and the features made available by the general purpose eSignature platforms (i.e. secure web conference, taylored workflows, etc).

We believe that professionals must be able to smootly move to fully digital and secure process without being force to become IT, LegalTech and Blockchain experts

We believe that the solution to be provided to them must focus on usability and ease of use, yet using the most advanced technology available.

## (Technical) Solution

GAYA shall be provided as a Cloud solution (SaaS) and will be available as a classic subscription service.

Gaya main components are:

- **a Web Application (available on a subscription basis)**

  this is a classic Web Application, providing a dashboard, user setting and account, and all the features to create and manage agreement signature dossier, including the invitation of al the paries and the verification status of theor identities. Initially, a limited number of dossier template shall be provided, particularly the LLC company incorporation flow.

- **a Mobile App, including:**

  - **a SSI wallet**
  - **a NFC identity Document Reader**

  the mobile app shall be available to the parties participating to the Agreement signature dossier. The App shall include SSI wallet feature, used when Gaya is acting as Credential Issuer on behalf of the professional (the Party).

  Further, in order to simplify the identity verification of the agreement participants, the professional shall be able to engage remotely with the electronic document of each participant, accessing via NFC and fetching/verifying the signed data.

- **an Internal WebConference tool**

  In order to provide maximum security, each GAYA customer (aka professional) shall be provided with the possibility to create agreement dedicated web conference room, where only the authorised participant will be able to enter.

  The tool shall provide the professional the possibility to archive the (encrypted) recording of the session.

  the webconference tool shall be integrated in the web application in a flat UIX so that the professional shall be in control of all the events involving the participant, at each stage of the agreement signature dossier.

- **a integrated back end SSI service for**

  - **verifiable credential validation (proof request, credential presentation validation, etc)**
  - **verifiable credential issuing (credential offer, credential request and issuing, etc)**

  Gaya will integrate the functionality of verifiable credentials issuance and verification, obviously based on the libraries made available by eSSIF-LAB.

- **File/Document sharing tool**

  a component that allows the professional to share, during the session, the documents to be signed with all the participants involved. Each participant will be able to see changes to the shared document in real time.

- **eSig, AES, QES service integration, where:**

  - **eSig shall be provided natively**
  - **AES shall be based on external services**
  - **QES shall be provided by QTSP partners**

  The eSig service shall be integrated in Gaya in all the eIDAS flavours. Of course, for what regards Qualified Electronic Signature, the service shall be provided by a Qualified trust Service provider.

- **Long term archival service integration**

  GAYA won't archive docyments on he long run, so in order to fulfill legal requirements, a legal long term archival service shall be integrated.

- **Audit trail and monitoring**

  Gaya shall archive all the relevant events and logs for each dossier, in order to support the professional and/or the participant in case of litigation or in standard audit procedure.

All the above listed components shall be developed and provided along the eSSIF-lab 1st BOC timeframe.

Furher, the following components shal be designed:

- **Workflow Composer**

  this components hall be intially available only at back office leveland then possibly extended to premium users. It will allows the customer to combine elementary task in a custom workflow.

- **Ricardian Contract Signing and deployment (initially on Ethereum)**

  this module shall be in charge of showing an agreement in the form of a Ricardian Contract (connected to a standard solidity smart contract) legally binding for the parties participating in the dossier. In a initial phase, the target platform shall be Ethereum.

## Integration

GAYA is a business application that is meant to fully leverage the SSI framework, both in the role of Issuer and Verifier.

GAYA shall be an issuer, when the participant isn't equipped with a trusted SSI wallet / identity credential

GAYA shall be a verifier, when the participant present an identity credential issued by another Trusted party.

the same approach shall be applied to eSignature services: gaya shall provide a native internal simple electronic signature service, while, in case a the participant is already equipped with both a SSI wallet and a associated eSignature Service, Gaya shall rely on the esternal service.

Gaya shall integrate with:

- CommercioKYC, that shal be integrated as a trusted issuer for Identity Credential
- SSI4DTM that shall be integrated as a trusted Advanced Electronic Signature provider



